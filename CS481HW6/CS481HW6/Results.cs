﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CS481HW6
{   //This class will be used to deseralize the json from the API
    public class Results
    {
         public List<Definitions> definitions { get; set; }
        public string word { get; set; }
        public string pronunciation { get; set; }
    }
    //This class is needed since it is a nested json that is given from the APi
    public class Definitions
    {
       public string type { get; set; }
        public string definition { get; set; }
        public string example { get; set; }
       public string image_url { get; set; }
    }
}
