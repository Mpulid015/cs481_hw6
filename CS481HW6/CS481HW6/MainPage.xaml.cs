﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using Plugin.Connectivity;
using CS481HW6;

namespace CS481HW6
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        HttpClient mysearchClient;
        public MainPage()
        {
            InitializeComponent();
            mysearchClient = new HttpClient();
            // example from https://github.com/jamesmontemagno/ConnectivityPlugin/blob/master/samples/ConnectivitySample/ConnectivitySample/App.cs
            //This will pop up when there is a change in the internet connection
            IsConnected.Text = CrossConnectivity.Current.IsConnected ? "Current Connection:Connected" : "Current Connection:Not Connected";
            CrossConnectivity.Current.ConnectivityChanged += (sender, args) =>
             {//show that there was a change in connection whether the app lost connection or if it got the connection back
                 DisplayAlert("connection changed", CrossConnectivity.Current.IsConnected?"Connected" :"Not connected"   , "OK");
                 //sets the label at the top of the app to show the current connection
                 IsConnected.Text = CrossConnectivity.Current.IsConnected ? "Current Connection:Connected" : "Current Connection:Not Connected";
             };

        }
        //This function will be used to make the GET request for the word that the user wants to know about
        private async void SearchButton_Clicked(object sender, EventArgs e)
        {//make sure the user enters something into the entry field
            if (!string.IsNullOrWhiteSpace(SearchWord.Text))
            {//check if there is a internet connection
                if (CrossConnectivity.Current.IsConnected)
                {//this is the main url for the api
                    string theurl = "https://owlbot.info/api/v4/dictionary/";
                    //set up the headers to set connection with api
                    mysearchClient.DefaultRequestHeaders.Add("User_Agent", "CS481 Definition app");
                    //the token needed to use the api
                    mysearchClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Token", "98e0aae2a1739fc083d29d6ee7c5a5c155bc460f");
                    //The myrequest is what is sent to the api with the word that the user wants to know about
                    HttpRequestMessage myrequest = new HttpRequestMessage();
                    //This adds the word the user wants to the main url
                    myrequest.RequestUri = new Uri(theurl + SearchWord.Text);
                    //sets it to be a get request
                    myrequest.Method = HttpMethod.Get;
                    //wait to see if there is a response
                    HttpResponseMessage mymessage = await mysearchClient.SendAsync(myrequest);
                    //Checks to see if it successed with the request
                    if (mymessage.IsSuccessStatusCode)
                    {//get the json that was returned into a string
                        string thecontent = await mymessage.Content.ReadAsStringAsync();
                        try//This try is to see if there is an error with the DeserializeObject function
                        {
                            //Turns the json object into a Results object
                            //make sure each field is available and not null and if null set a certain phrase to explain
                            Results results = JsonConvert.DeserializeObject<Results>(thecontent);
                            if (results.definitions.First().type != null)
                                Thetype.Text = "The type is " + results.definitions.First().type;
                            else
                                Thetype.Text = "There is no type available";
                            if (results.definitions.First().definition != null)
                                theDef.Text = "The definition is " + results.definitions.First().definition;
                            else
                                theDef.Text = "There is no definition available";
                            if (results.definitions.First().example != null)
                                theexample.Text = "An example would be " + results.definitions.First().example;
                            else
                                theexample.Text = "There is no example available";
                            if (results.definitions.First().image_url != null)
                                theimage.Source = results.definitions.First().image_url;
                            else
                                theimage.Source = null;
                            if (results.word != null)
                                theword.Text = "The word is " + results.word;
                            else
                                theword.Text = "The word is not available";
                            if (results.pronunciation != null)
                                thepronunciation.Text = "The pronunciation of the word is " + results.pronunciation;
                            else
                                thepronunciation.Text = "The pronunciation of the word is not available";
                        }
                        catch (Exception exp)
                        {//Shows what the exception was that caused it to come here and display on the app
                            Console.WriteLine(exp.Message);
                            //Need to show the user the error and make the rest of field "" so they don't show previously searched fields
                            Thetype.Text = "There was a problem with the json exception message:" + exp.Message;
                            theDef.Text = "";
                            theexample.Text = "";
                            theword.Text = "";
                            thepronunciation.Text = "";
                            theimage.Source = "errorimage.jpg";
                        }

                    }
                    else
                    {//This is if there was something wrong with the request and it shows the error message received from the api

                        Thetype.Text = "There was a problem with the request " + mymessage.StatusCode.ToString();
                        theDef.Text = "";
                        theexample.Text = "";
                        theword.Text = "";
                        thepronunciation.Text = "";
                        theimage.Source = "errorimage.jpg";
                    }
                }
                else
                {//This will show because there was a problem with the internet connection during the time of the request
                    Thetype.Text = "There is no internet connection";
                    theDef.Text = "";
                    theexample.Text = "";
                    theword.Text = "";
                    thepronunciation.Text = "";
                    theimage.Source = "errorimage.jpg";
                }

            }
            else
            {//shows when the user left the entry text empty
               await DisplayAlert("Problem", "Please enter a word", "ok");
            }
            
           
        }
        //used to have a button to test connection manually
        ////This function will determine if the app does have internet connection and deisplay it in the Label IsConnected
        //private void TestInternet_Clicked(object sender, EventArgs e)
        //{
        //    IsConnected.Text = CrossConnectivity.Current.IsConnected ? "Connected" : "Not Connected";
        //}
    }
}
